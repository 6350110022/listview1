import 'package:flutter/material.dart';

class Example2 extends StatelessWidget {
  const Example2({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("ListView2"),
      ),
      body: ListView(
        children: [
          ListTile(
            leading: Icon(
              Icons.directions_railway,
              size: 50,
            ),
            title: Text(
                '7.00 A.M.',
                style: TextStyle(fontSize: 18),
            ),
            subtitle: Text(
              'Hello World',
              style: TextStyle(fontSize: 15),
            ),
            trailing: Icon(
                Icons.notifications_none,
                size: 25,
            ),
            onTap: (){
              print("Train");
            },
          ),
          ListTile(
            leading: Icon(
              Icons.directions_bike,
              size: 50,
            ),
            title: Text(
              '8.00 A.M.',
              style: TextStyle(fontSize: 18),
            ),
            subtitle: Text(
              'Hello World',
              style: TextStyle(fontSize: 15),
            ),
            trailing: Icon(
              Icons.notifications_none,
              size: 25,
            ),
            onTap: (){
              print("Bike");
            },
          ),
          ListTile(
            leading: Icon(
              Icons.directions_bus,
              size: 50,
            ),
            title: Text(
              '9.00 A.M.',
              style: TextStyle(fontSize: 18),
            ),
            subtitle: Text(
              'Hello World',
              style: TextStyle(fontSize: 15),
            ),
            trailing: Icon(
              Icons.notifications_none,
              size: 25,
            ),
            onTap: (){
              print("Bus");
            },
          ),
          ListTile(
            leading: Icon(
              Icons.directions_boat,
              size: 50,
            ),
            title: Text(
              '10.00 A.M.',
              style: TextStyle(fontSize: 18),
            ),
            subtitle: Text(
              'Hello World',
              style: TextStyle(fontSize: 15),
            ),
            trailing: Icon(
              Icons.notifications_none,
              size: 25,
            ),
            onTap: (){
              print("Boat");
            },
          ),
          ListTile(
            leading: Icon(
              Icons.directions_car,
              size: 50,
            ),
            title: Text(
              '11.00 A.M.',
              style: TextStyle(fontSize: 18),
            ),
            subtitle: Text(
              'Hello World',
              style: TextStyle(fontSize: 15),
            ),
            trailing: Icon(
              Icons.notifications_none,
              size: 25,
            ),
            onTap: (){
              print("Car");
            },
          ),
          ListTile(
            leading: Icon(
              Icons.directions_walk,
              size: 50,
            ),
            title: Text(
              '12.00 A.M.',
              style: TextStyle(fontSize: 18),
            ),
            subtitle: Text(
              'Hello World',
              style: TextStyle(fontSize: 15),
            ),
            trailing: Icon(
              Icons.notifications_none,
              size: 25,
            ),
            onTap: (){
              print("Walk");
            },
          ),
          ListTile(
            leading: Icon(
              Icons.directions_transit,
              size: 50,
            ),
            title: Text(
              '8.00 A.M.',
              style: TextStyle(fontSize: 18),
            ),
            subtitle: Text(
              'Hello World',
              style: TextStyle(fontSize: 15),
            ),
            trailing: Icon(
              Icons.notifications_none,
              size: 25,
            ),
            onTap: (){
              print("Train");
            },
          ),
          ListTile(
            leading: Icon(
              Icons.directions_off,
              size: 50,
            ),
            title: Text(
              '8.00 A.M.',
              style: TextStyle(fontSize: 18),
            ),
            subtitle: Text(
              'Hello World',
              style: TextStyle(fontSize: 15),
            ),
            trailing: Icon(
              Icons.notifications_none,
              size: 25,
            ),
            onTap: (){
              print("Train");
            },
          ),
          ListTile(
            leading: Icon(
              Icons.directions_rounded,
              size: 50,
            ),
            title: Text(
              '8.00 A.M.',
              style: TextStyle(fontSize: 18),
            ),
            subtitle: Text(
              'Hello World',
              style: TextStyle(fontSize: 15),
            ),
            trailing: Icon(
              Icons.notifications_none,
              size: 25,
            ),
            onTap: (){
              print("Train");
            },
          ),
          ListTile(
            leading: Icon(
              Icons.directions_ferry,
              size: 50,
            ),
            title: Text(
              '8.00 A.M.',
              style: TextStyle(fontSize: 18),
            ),
            subtitle: Text(
              'Hello World',
              style: TextStyle(fontSize: 15),
            ),
            trailing: Icon(
              Icons.notifications_none,
              size: 25,
            ),
            onTap: (){
              print("Train");
            },
          ),
        ],
      ),
    );

  }
}
